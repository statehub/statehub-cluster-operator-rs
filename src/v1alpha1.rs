//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use k8s_openapi::apiextensions_apiserver::pkg::apis::apiextensions::v1::CustomResourceDefinition;
use kube::CustomResource;
use kube::CustomResourceExt;
// use kube::ResourceExt;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize, CustomResource, JsonSchema)]
#[kube(
    kind = "StatehubCluster",
    group = "statehub.io",
    version = "v1alpha1",
    shortname = "shc"
)]
pub struct ClusterSpec {
    pub namespace: String,
}

impl StatehubCluster {
    pub fn singleton(_name: &str) -> CustomResourceDefinition {
        // let crd = Self::crd();
        // let mut version = crd.spec;
        Self::crd()
    }
}

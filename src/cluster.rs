//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::fmt;

use k8s_openapi::{api::core::v1::Namespace, apimachinery::pkg::apis::meta::v1::OwnerReference};
// use k8s_openapi::api::core::v1::{ConfigMap, Namespace, Node, Pod, Secret};
// use serde_json as json;
use kube::client;
use kube::{Resource, ResourceExt};

use super::ClusterSpec;
use super::StatehubCluster;

mod helper;

pub struct Kubectl {
    name: String,
    spec: ClusterSpec,
    client: client::Client,
}

impl Kubectl {
    pub fn new(client: client::Client, statehub: &StatehubCluster) -> Self {
        let name = statehub.name();
        let spec = statehub.spec.clone();
        Self { name, spec, client }
    }

    pub async fn create_namespace(&self) -> kube::Result<Namespace> {
        // let namespace = json::from_value(json::json!({
        //     "apiVerion": "v1",
        //     "kind": "Namespace",
        //     "metadata": {
        //         "name": namespace,
        //     }
        // }))?;
        let owner = OwnerReference {
            api_version: StatehubCluster::api_version(&()).to_string(),
            kind: StatehubCluster::kind(&()).to_string(),
            name: self.name.clone(),
            ..OwnerReference::default()
        };
        let mut namespace = Namespace::default();
        namespace.metadata.name = Some(self.spec.namespace.clone());
        namespace.metadata.owner_references.push(owner);
        let pp = self.post_params();
        self.api::<Namespace>().create(&pp, &namespace).await
    }

    pub async fn delete_namespace(&self) -> kube::Result<()> {
        let dp = self.delete_params();
        self.api::<Namespace>()
            .delete(&self.spec.namespace, &dp)
            .await?
            .map_left(|object| log::trace!("Deleting namespace: {:?}", object.status))
            .map_right(|status| log::trace!("Deleted namespace: {:?}", status));
        Ok(())
    }

    pub async fn check_namespace(&self) -> kube::Result<Namespace> {
        self.api::<Namespace>().get(&self.spec.namespace).await
    }
}

impl fmt::Debug for Kubectl {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Kubectl")
            .field("name", &self.name)
            .field("spec", &self.spec)
            .field("client", &"kube::Client")
            .finish()
    }
}

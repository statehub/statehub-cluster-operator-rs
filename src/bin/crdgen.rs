//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

#![cfg_attr(feature = "pedantic", warn(clippy::pedantic))]
#![warn(clippy::use_self)]
#![warn(clippy::map_flatten)]
#![warn(clippy::map_unwrap_or)]
#![warn(deprecated_in_future)]
#![warn(future_incompatible)]
#![warn(unreachable_pub)]
#![warn(missing_debug_implementations)]
#![warn(rust_2018_compatibility)]
#![warn(rust_2018_idioms)]
#![warn(unused)]
#![deny(warnings)]

use kube::CustomResourceExt;
use serde_yaml as yaml;

use statehub_cluster_operator::StatehubCluster;

fn main() -> anyhow::Result<()> {
    let crd = dbg!(StatehubCluster::crd());
    let text = yaml::to_string(&crd)?;
    println!("{}", text);
    Ok(())
}

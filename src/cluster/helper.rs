//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use kube::api;

use super::*;

impl Kubectl {
    pub(super) fn delete_params(&self) -> api::DeleteParams {
        api::DeleteParams {
            grace_period_seconds: Some(0),
            ..api::DeleteParams::default()
        }
    }

    pub(super) fn _list_params(&self) -> api::ListParams {
        api::ListParams::default()
    }

    pub(super) fn post_params(&self) -> api::PostParams {
        api::PostParams::default()
    }

    pub(super) fn api<T>(&self) -> api::Api<T>
    where
        T: kube::Resource,
        <T as kube::Resource>::DynamicType: Default,
    {
        let client = self.client.clone();
        api::Api::<T>::all(client)
    }

    pub(super) fn _namespaced_api<T>(&self, namespace: &str) -> api::Api<T>
    where
        T: kube::Resource,
        <T as kube::Resource>::DynamicType: Default,
    {
        let client = self.client.clone();
        api::Api::<T>::namespaced(client, namespace)
    }
}
